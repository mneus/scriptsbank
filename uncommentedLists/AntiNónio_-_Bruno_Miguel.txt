||expresso.sapo.pt^$inline-script,important
||rr.sapo.pt^$inline-script,important
||blitz.pt^$inline-script,important
||visao.pt^$inline-script,important
||expressoemprego.pt^$inline-script,important
||exameinformatica.pt^$inline-script,important
||jornaldenegocios.pt^$inline-script,important
||sabado.pt^$inline-script,important
||tsf.pt^$inline-script,important
||radiocomercial.iol.pt^$inline-script,important
||maisfutebol.iol.pt^$inline-script,important
||tvi24.iol.pt^$inline-script,important
||cloud.weborama.design/*.js$important;
||aminhaconta.xl.pt/*.js$important
||cdn.xl.pt/sso/js/CofinaSSOApi.js$important
||cdn.iol.pt/js/utils/blockadblock.js$important
||cdn.iol.pt/utils/NonioContentGatting/js/*.js$important
||radiocomercial.iol.pt/scripts/vendor/NonioContentGatting/*$first-party,important
||cidade.iol.pt/scripts/vendor/noniocontentgatting/js/noniocontentbar.js$important
||m80.iol.pt/scripts/vendor/NonioContentGatting/*$important
tvi24.iol.pt###nonio-basiclogin
iol.pt###nonio-basiclogin
||cdn.iol.pt/BarraIOL/dist/main.js
||id.impresa.pt/js/frame.html$important
||static.impresa.pt/content-gate/active/content-gate.min.js$important
||id.impresa.pt/js/impresa-sso.min.js$important
||*/noniogating/*.js$important
||*/noniogatting.js$important
||*/nonio.js$important
