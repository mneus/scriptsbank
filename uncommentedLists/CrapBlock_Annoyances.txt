tomshardware.com/news/*.html?ajax=1$xhr,domain=tomshardware.com
forbes.com/forbesapi/content/uri.json?*uri=*$xhr,domain=forbes.com
forbes.com/forbesapi/stats/topStories.json$xhr,domain=forbes.com
||techwalla.com/infinite-scroll/$xhr
||venturebeat.com/infinite$xhr
/articleshowajax/*.cms?$xhr
graph.bustle.com/?query=*&variables=*like*$xhr
vice.com/api/v1/related/articles/*?*&per_page=5
thenextweb.com/wp-content/themes/cyberdelia/ajax/partials/article-sheet.php?frstPostId=*&*&_=*
https://www.dailydot.com/*/*/?ajax$xhr
androidcentral.com/ajax/article/related/*?json$xhr,domain=androidcentral.com
www.androidcentral.com##.btn--load-more--active.js-load-more.btn--load-more.btn > .fa-spin
https://www.cnet.com/news/*/xhr/$xhr,domain=cnet.com
www.cnet.com##.upNextBottomWrapper
www.cultofmac.com##+js(abort-on-property-write.js, COM_Infinite)
9to5mac.com/?infinity=scrolling$xhr
/?infinity=scrolling$xhr
tomsguide.com/xhr/*.html?page=*$xhr
theintercept.com##.InfiniteScroll
||fc-api.fastcompany.com/api/v2/fastcompany/post-finite$xhr
theintercept.com##.PostLanding > div:nth-of-type(2)
theintercept.com/api/requestReverseChron/?language=en&page=*&postsPerPage=1$xhr
imgur.com##+js(addEventListener-defuser.js, keydown)
www.bbc.com##.sticky-player--visible.sticky-player--pinned.sticky-player--enabled
twitter.com##.js-moments-tab.moments
twitter.com##.Footer-adsModule
twitter.com##.import-prompt.flex-module
twitter.com##.trends.Trends.module
twitter.com##.roaming-module.module.Footer:has(.copyright.Footer-copyright.Footer-item)
twitter.com##.is-autoCentered.is-forceRight.dropdown-menu--rightAlign.dropdown-menu.DashUserDropdown > * > * > [href^="https://ads.twitter.com/"]
twitter.com##.trends.Trends.module:has-text(Worldwide trends)
disqus.com###reactions
||seal.websecurity.norton.com
||seal.godaddy.com
||seal.globalsign.com
##[href^="https://www.positivessl.com/trusted-ssl-site-seal.php"]
/comodo_secure_seal_*.png
www.ssl.com##[href^="https://www.ssl.com/site_seals/"]
www.wsj.com###cx-candybarhp
www.wsj.com###cx-candybar
www.wsj.com##[href*="https://buy.wsj.com"]
www.wsj.com###cx-mktg-tile:has(a[href*="https://buy.wsj.com"])
www.wsj.com##[class^="style__subscribe"]
www.wsj.com###cx-scrim-overlay
www.wsj.com##.bing-native-ad
forbes.com/statsapi
forbes.com/consent/?toURL=https://www.forbes.com/favicon.ico$image
||sc-static.net/scevent.min.js
##div.ytp-pause-overlay
##.ytp-button.ytp-watch-later-button
##.ytp-share-button.ytp-button
##.ytp-watch-later-button
translate.google.com###t-new-user > .cp-promo-c-
translate.google.com###gt-promo-lr
||ssl.gstatic.com/translate/community/promo/promo-new-1x.png$image
||ssl.gstatic.com/translate/community/promo/promo-new-2x.png$image
translate.google.com###gt-pb-star
##.grecaptcha-badge
||www.facebook.com/v2.5/plugins/like.php$subdocument
||www.facebook.com/*/plugins/like.php$subdocument
||www.facebook.com/plugins/like.php$subdocument
##iframe[src^="https://www.facebook.com/plugins/like.php"]
browsehappy.com##.likebutton
||www.facebook.com/plugins/like.php$domain=bgp.he.net
||platform.twitter.com$domain=keskustelu.skepsis.fi
||connect.facebook.net$domain=keskustelu.skepsis.fi
keskustelu.skepsis.fi##.twitter-share-button
||google.com/js/client:plusone.js$third-party
||google.com/js/plusone.js$third-party
||apis.google.com/*/+1/sharebutton$subdocument
||apis.google.com/*/+1/fastbutton$subdocument
||pinterest.com/js/pinit.js$third-party
yle.fi##.yle__newsletter--uutiset.yle__newsletter
disroot.org/pwm-disroot/public/resources/themes/*-bg.jpg
||4.bp.blogspot.com/-sQeomkRCqzQ/WL7MLVD4rYI/AAAAAAAAZLE/71VprPZvgbUqfNZoVPF4cy2EHLEDZqLMwCLcB/s1600/be-a-nice-guy.png$image
www.lapinkansa.fi###yleisnosto_ajax-8 > .topic-item > .textarea-container > .mainos-fluid-container
kokeile.il.fi##.parade-ad-container
www.wunderground.com##.pane-wu-fullscreenweather-ad-box-atf
ilmailukartta.fi##.ad
www.ghacks.net##.ghacks_ad_code
thomashunter.name###carbon-wrapper
www.techhive.com###articleLeaderboardWrapper
##li.disqus-footer__item.install
##li.disqus-footer__item.privacy
www.iltalehti.fi##.nakoislehti
wilma.turku.fi##.QR-code.infos-block.panel
wilma.tnk.utu.fi##.QR-code.infos-block.panel
reddit.com/*/pixel.png
redditmedia.com/moat/
old.reddit.com/web/log/error.json
||checkout.hidemyass.com/affiliate.php^$third-party
arstechnica.com###ars-sub-app-no-banners-no-tracking-animated
arstechnica.com###ars-sub-app-free-yubikey
||xda-developers.com/images/XDA2013/header/swappa.png$image
||xda-developers.com/images/XDA2013/header/themer_social_white.png$image
||xda-cdn.com/files/*/leaseweb_*.png$image
||xda-developers.com/images/leaseweb_*.png$image
||forum.xda-developers.com/sponsors/brave/brave_desk.png$image
www.tori.fi###prisjakt_popular_240x350
www.tori.fi###panorama_top
www.tori.fi##.pull-right.options_right > div.clear.menu_item:has(.share_on_facebook.menu_item_link)
theverge.com###privacy-consent
www.theverge.com##.m-privacy-consent__inner
||www.viheraho.info/autonvaraosastore728x90.gif$image
www.is.fi##.is-advertorial.external.full-width.teaser
www.flashback.org###top-banner-container
||betsonsport.ru/banners/$subdocument
reddit.com##.redesignbetabar-js.redesignNightmodeBar.onboardingbar.infobar
reddit.com##.listingsignupbar.infobar
reddit.com##.commentsignupbar.infobar
reddit.com##.onboardingbar.subscribebar.infobar
reddit.com##.ProfileVoiceAd
reddit.com##.giftgold:has(.access-required)
reddit.com##.trending-subreddits
||reactandshare.com^$third-party
yastatic.net/share$script
duckduckgo.com,3g2upl4pq6kufc4m.onion##.footer
duckduckgo.com,3g2upl4pq6kufc4m.onion##.tag-home__item
duckduckgo.com,3g2upl4pq6kufc4m.onion##.js-hl-twitter.header--aside__twitter.header--aside__item
duckduckgo.com,3g2upl4pq6kufc4m.onion##.header__label.showcase.header--aside__item
duckduckgo.com,3g2upl4pq6kufc4m.onion##.js-hl-button.header__label.header__clickable.js-popout.popout-trig > .ddgsi-down.ddgsi.js-popout-link
duckduckgo.com,3g2upl4pq6kufc4m.onion##.nav-menu--social-media
duckduckgo.com,3g2upl4pq6kufc4m.onion##.badge-link--newsletter
duckduckgo.com,3g2upl4pq6kufc4m.onion##.badge-link--newsletter.badge-link--thumbnail.js-badge-link.badge-link--top.badge-link
duckduckgo.com,3g2upl4pq6kufc4m.onion##.atb-banner
duckduckgo.com,3g2upl4pq6kufc4m.onion##.js-onboarding-ed.onboarding-ed
duckduckgo.com/assets/onboarding/
3g2upl4pq6kufc4m.onion/assets/onboarding/
duckduckgo.com,3g2upl4pq6kufc4m.onion##.logo_homepage__tt
@@duckduckgo.com/assets/icons/thirdparty/
@@3g2upl4pq6kufc4m.onion/assets/icons/thirdparty/
duckduckgo.com,3g2upl4pq6kufc4m.onion#@#.social__link
duckduckgo.com,3g2upl4pq6kufc4m.onion#@#.social__icon
duckduckgo.com,3g2upl4pq6kufc4m.onion#@#a[title="Twitter"]
duckduckgo.com,3g2upl4pq6kufc4m.onion#@#a[title="Facebook"]
duckduckgo.com,3g2upl4pq6kufc4m.onion#@#a[title="Instagram"]
duckduckgo.com,3g2upl4pq6kufc4m.onion#@#a[title="YouTube"]
duckduckgo.com##[href="https://duckduckgo.com/app"]
www.startpage.com##.hero-search__learn-more
www.startpage.com###more-content
www.startpage.com##.search-item.card.promotion:has-text(It's your data)
@@||slack-edge.com/*/img/services/$image
@@||slack-edge.com/*/marketing/img/$image
@@toneden.io/production/images/link-services/$image
@@proxy.duckduckgo.com/ip3/$image
@@cbsistatic.com/img/*instagram.jpg$domain=cnet.com
@@ctrl.blog/assets/svg/$image
yle.fi##DIV[class^=TextBanner]:has(DIV[class^=SubscriptionForm__container__]:has-text(yle.fi-uutiskirje))
io-tech.fi#@#.sidebar-widgets
www.ctrl.blog##.boxa:has(small:has-text(Advertisement))
www.ctrl.blog##.cta-feedly.page-footer-cta
www.ctrl.blog##.entry-header>aside:has(a[href="https://flattr.com/contributors"]):has-text(Become a patron of the web.)
www.ctrl.blog##.entry-header>aside:has-text(Please disable your blocker)
www.ctrl.blog##.entry-header>aside:has-text(Please consider disabling your ad blocker. It takes human time and effort to write this and ads pay the bills.)
www.ctrl.blog##.entry-header>aside:has-text(Hey! I just met you; and this is crazy; but whitelist me in adblock, maybe?)
www.ctrl.blog##.entry-header>aside:has-text(Your internet bill doesn’t pay the websites you read.):has([href="https://flattr.com/contributors"])
www.ctrl.blog##.entry-header>aside:has-text(Your internet bill doesn’t pay the blogs you read.):has([href="https://flattr.com/contributors"])
www.ctrl.blog##.auc-rm.auc-bu.box-pr.box-pf.boxa:has(iframe[src*="amazon-adsystem.com"])
palemoon.org##div:has(#abclosebutton):has-text(please disable your ad blocker)
www.hs.fi#@#.follow-text
developer.mozilla.org##.notification-tray:has-text(survey)
www.aamulehti.fi##DIV[id^=promo]:has(a[href="https://uutiskirje.aamulehti.fi/"]):has(img)
www.metrolyrics.com##.driver-related
bbs.io-tech.fi##img[src^="https://www.io-tech.fi/bnneri"]
gravatar.com/js/gprofiles.js
www.telia.fi##DIV[id^=notification-]:has-text(evästeitä)
@@||api.quad9.net/search/
speedtest.net##.eot-dd.pure-g:has(.svg-icon-downdetector-logo)
||cdns2.freepik.com/img/campaigns/flashsales/banner/background.jpg$image
||cdns2.freepik.com/img/campaigns/flashsales/popup/1.png$image
www.freepik.com###banner-flashsales-1
www.freepik.com###popup-flashsales-1
www.freepik.com###header:style(-webkit-filter:blur(0px) !important; -moz-filter:blur(0px) !important; -ms-filter:blur(0px) !important; -o-filter:blur(0px) !important; filter:blur(0px) !important;)
www.freepik.com###footer:style(-webkit-filter:blur(0px) !important; -moz-filter:blur(0px) !important; -ms-filter:blur(0px) !important; -o-filter:blur(0px) !important; filter:blur(0px) !important;)
www.freepik.com##.single-column:style(-webkit-filter:blur(0px) !important; -moz-filter:blur(0px) !important; -ms-filter:blur(0px) !important; -o-filter:blur(0px) !important; filter:blur(0px) !important;)
www.freepik.com##.top-banner:style(-webkit-filter:blur(0px) !important; -moz-filter:blur(0px) !important; -ms-filter:blur(0px) !important; -o-filter:blur(0px) !important; filter:blur(0px) !important;)
userstyles.org##.android_button_banner
userstyles.org###top_android_button:has-text(Stylish on Android)
userstyles.org##div.us-stylecard--short:has(.fallbackDiv:has(.fallbackImg[src*="/ui/images/280x294.png"]))
userstyles.org##.overlay_background
userstyles.org##.android_walking
userstyles.org##.footsteps
userstyles.org##.walking
userstyles.org##[href^="/categories/android"]
userstyles.org##.android_button_button
||userstyles.org/ui/images/icons/android*$image,important
userstyles.org###paypal_form
userstyles.org##div.bottomLinks:has(div:has-text(Browsers:))
@@userstyles.org/ui/images/icons/$image
##[href^="https://chrome.google.com/webstore/detail/stylish-custom-themes-for/fjnbnpbmkenffdnngjfgmeleoegfcffe"]
##[href^="https://addons.mozilla.org/en-US/firefox/addon/stylish"]
##[href^="https://addons.opera.com/en-gb/extensions/details/stylish"]
||chrome.google.com/webstore/detail/stylish-custom-themes-for/fjnbnpbmkenffdnngjfgmeleoegfcffe^$document
||addons.opera.com/en/extensions/details/stylish-for-opera/^$document
##[href*="go.nordvpn.net"]
##a[href^="https://www.get-express-vpn.com/"]
||dnsleaktest.com/img/ivpn-sponsor.png$image
##a[href*="NordVPN.com"]
##[href^="https://www.privateinternetaccess.com/pages/buy-vpn/"]:has(img)
||freenode.net/static/img/logos/PIALogo_white.svg$image
freenode.net##[href^="https://www.privateinternetaccess.com/pages/buy-vpn/"]
hooktube.com##[href="/nordvpn"]
hooktube.com##[href^="/static/nordvpn-"]
lulz.com##[href="https://lulz.com/nordvpn"]
||cryptoip.info/assets/images/liquidvpn-logo.gif$image
torrentz2.eu##.alert-danger:has-text(PLEASE protect your online privacy.):has-text(IP-LOGGED)
/pia_banner.$image
power.fi##.ng-scope.notification-newsletter
www.linode.com##.library-signup > .email-signup
portforward.com##[href="https://portforward.com/store/pfconfig.cgi"]
@@||cdn.ampproject.org^$script,domain=independent.co.uk|draugeros.ml|sumguy.com
facebook.com###u_0_f:has-text(See more of ):has-text( on Facebook)
distrowatch.com##a[href^="https://www.elastix.org/free-cloud-pbx/"]
distrowatch.com##td:nth-of-type(3) > table:has-text(Sidebar Sponsor)
distrowatch.com##tr:nth-of-type(15) > .News1 > .News > tbody:has(.NewsDate:has-text(Sponsored Listing))
@@*$generichide,domain=poedb.tw
@@||poedb.tw/js/ads.js
seekingalpha.com##.in.fade.modal:has(.modal-body:has-text(to finish reading this article):has-text(Already Registered?))
seekingalpha.com##body:style(overflow: auto !important;)
www.theguardian.com##.hide-until-mobile.new-header__cta-bar:has-text(Support The):has-text(Guardian)
www.theguardian.com##.contributions__epic
||adblock.keweon.center^
||keweon.center^
@@sec.keweon.center/dns-query
/favicon.ico$domain=n.kapsi.fi
@@||buttons.github.io/buttons.js$domain=github.io
www.is.fi##.ad-contact
stackoverflow.com##li > .nav-links:has-text(Teams):has(a[href*="/teams"])
directory.fsf.org##[src="//static1p.fsf.org/nosvn/banners/2018fundraiser/"]
directory.fsf.org##div#mw-head:style(margin-top: auto !important;)
directory.fsf.org##div#mw-panel:style(margin-top: auto !important;)
directory.fsf.org##div#.client-js:style(height: auto;)
www.fsf.org##iframe[src="//static1p.fsf.org/nosvn/banners/2018fundraiser/"]
www.defectivebydesign.org##iframe[src="//static.defectivebydesign.org/nosvn/banners/2018fundraiser/"]
###fsf-modal-window-elem-container
||pastebin.com/zip/$subdocument
||pastebin.com/i/steadfast_1.png$image
||pastebin.com/i/index_bar_christmas.png$image
pastebin.com###blackfridaynotice
pastebin.com###content_right > div:has-text(gift a PRO account)
www.ghacks.net##div.border--top--gray:has-text(We need your help)
www.openstreetmap.org##.visible.welcome
github.com##[href^="https://opencollective.com"][href*="/sponsor/"]:has(img)
@@||marketo.com$domain=about.gitlab.com
@@||marketo.net$domain=about.gitlab.com
@@marketo.com$domain=marketo.com
@@*$generichide,domain=gitlab.com|gitlab.lelux.fi|git.synz.io
@@||embed.tawk.to^$script,domain=buyvm.net
#@#.twitter-timeline
@@||twitter.com/js/timeline.$third-party,script
hive.co#@#[data-cookie-id]
@@||facebook.net$domain=hive.co
@@||facebook.com$domain=hive.co
@@*$generichide,domain=hive.co
bloomberg.com##.share-article-button
bloomberg.com###paywall-banner
www.wired.com##.paywall-container-component
||rutracker.org/iframe/MG-bottom.html$subdocument
rutracker.org###bn-idx-marathonbet
rutracker.org###bn-idx-3
rutracker.org###bn-idx-r1
rutracker.org##.vMiddle
rutracker.org##.w100.tCenter
rutracker.org##[id^="bn-idx-"]
rutracker.org##[href="https://rublacklist.net/rutracker_redirect"]
||robinbob.in$domain=rutracker.org
||rublacklist.net$domain=rutracker.org
||roskomsvoboda.org$domain=rutracker.org
rutracker.org###soc-container
mashable.com##.follow-channel
www.deepl.com##.dl_footer__social
itsfoss.com##.ss-share-sidebar-total-shares.ss-total-shares.ss-total-counter
itsfoss.com##.ss-inline-share-content
itsfoss.com##.ss-social-share-label
about.gitlab.com##.article.blog > * > .social
www.arabstoday.net##.newread-social
fstoppers.com##.follow-fstoppers
www.alpro.com##.social
www.paypal.com##div[class^="share_campaign__share-icons"]
asunnot.oikotie.fi##.some-links
motherboard.vice.com###Facebook
motherboard.vice.com##.m-r-5-xs > .site-header__social-icon
itsfoss.com##.ss-follow-total-counter
www.mnn.com##.square-social
www.cnbc.com###social-buttons
www.eff.org##[href^="https://plus.google.com/+"]
##.valign-wrapper.hide-on-med-and-down.page-footer.left.darken-1.deezloader-black
qz.com##._07fe7:has-text(Please read our privacy policy.)
www.gsmarena.com###cdn-hosting
gsmarena.com/w/css/maxcdn.gif
gsmarena.com/counter-js.php3
indianexpress.com##aside.o-story-content__related:has-text(You might like)
www.howtogeek.com###div-thetop1:has(script[src*="/pagead/js/"])
androidfilehost.com##.page-file-thanks.page-container.download-file.row > .col-md-4:has-text(advertisement:)
mt-tech.fi###custom_html-8:has(.widget-title:has-text(Ad:))
debian-administration.org##.advert
mailbox.org##.block.nav-testen.mod_customnav:has-text(Free):has-text(30-day):has-text(trial)
www.youtube.com#@#a[title="Instagram"]
lawnchair.app#@#.mdi-twitter
www.foobar2000.org##.padding > table:has(caption:has-text(advertisement))
www.foobar2000.org##.release_info > table:has(caption:has-text(advertisement))
ghacks.net/wp-content/uploads/2005/10/ghacks-technology-news.jpg$domain=www.ghacks.net,image
www.ghacks.net##div > div.snippet-label-img:has-text(Logo)
youtube.com#@#.ytp-ce-element
www.wired.co.uk##.c-top-stories-lazy-load__spinner
||blog.linuxmint.com/pictures/twitter.png$image
||blog.linuxmint.com/pictures/facebook.png$image
www.tecmint.com###quickiebarpro
linuxize.com##.block.affiliate
www.namepros.com##.sidebar > * > [href^="https://www.escrow.com/"]
www.whonix.org##.alert:has-text(Whonix requires payments to stay alive.)
||www.whonix.org/w/images/d/d0/Donate50.png$image
www.whonix.org###localNotice:has-text(Whonix requires money to stay alive.)
robertheaton.com##.blogpost.post > section:has(#mc-embedded-subscribe-form)
robertheaton.com##.container > section:not(.blogpost):has(#mc-embedded-subscribe-form)
www.circleid.com##.newsletterSignUp
about.me##.anonymous_ledge_view-scope-1RSnD
linuxmint.com###sidebar
ifttt.com#@#img[title="YouTube"]
ifttt.com#@#img[title="Twitter"]
ifttt.com#@#img[title="Instagram"]
ifttt.com#@#img[title="Facebook"]
ifttt.com#@#img[title="Pinterest"]
ifttt.com#@#img[title="Flickr"]
ifttt.com#@#img[title="LinkedIn"]
ifttt.com#@#img[alt="Flickr"]
ifttt.com#@#img[alt="Twitter"]
ifttt.com#@#img[alt="Pinterest"]
ifttt.com#@#img[alt="Facebook"]
ifttt.com#@#img[alt="Instagram"]
ifttt.com#@#img[alt="LinkedIn"]
ask.fedoraproject.org###no-javascript
en.wikipedia.org##[href^="https://donate.wikimedia.org/wiki/Special:FundraiserRedirector"]
en.wikipedia.org##[href="//shop.wikimedia.org"]
##.blockchain-btn
www.discogs.com##.gs_container_column.homepage-registration
www.datacenterdynamics.com##div.a2a_default_style.a2a_kit_size_32.a2a_kit.a2a
hinta.fi##.hv-wrapper-left-i > [href*=".php"]:has(img)
||www.nytimes.com/interactive/2018/admin/100000004799217.embedded.html$subdocument
who.is##.col-md-4 > div.row:nth-of-type(2) > .col-md-12:has(.text-centered.queryResponseHeaderSide:has-text(Suggested Domains for ))
who.is##center:has(img[src="https://d7l9sro0h1dem.cloudfront.net/results_page_top_logo.jpg"]):has([href="https://www.name.com/redirect/whodotis/sidebar-top"])
who.is##center:has(img[src="https://d7l9sro0h1dem.cloudfront.net/results_page_bottom_logo.jpg"]):has([href="https://www.name.com/redirect/whodotis/sidebar-bottom"])
who.is###domainAvailabilityPowerBarProgress
who.is##.teaser-bar.container-fluid:has([href^="http://www.us3.list-manage.com/subscribe"])
||d7l9sro0h1dem.cloudfront.net/homepage_banner.jpg$image
||www.posti.fi/henkiloasiakkaat/seuranta/assets/images/adwidget-placeholder.jpg$image
www.posti.fi##.adwidget-placeholder
translate.google.com###gb
time.is##.adjustright.faded.tr.noprint > .tr.noprint:has-text(Share this page)
time.is###restaurants
time.is###follow_app
medium.com,medium.freecodecamp.org,hackernoon.com#@#.svgIcon--twitterFilled
medium.com,medium.freecodecamp.org,hackernoon.com#@#.svgIcon--facebookFilled
medium.com,medium.freecodecamp.org,hackernoon.com#@#.js-followState
medium.com,medium.freecodecamp.org,hackernoon.com#@#.js-postShareWidget
medium.com,medium.freecodecamp.org,hackernoon.com##.js-bookmarkButton.button--bookmark.button--withSvgIcon
medium.com##a[href="https://medium.com/membership?source=upgrade_membership---nav_full"]
dapi.videoly.co/*/event/
securityheaders.com##div.reportSection:has(.reportTitle:has-text(Other Services))
securityheaders.com##h3:has-text(Sponsored by):has([href^="https://www.sophos.com/"])
securityheaders.com###sponsor-footer
||www.macobserver.com/wp-content/themes/observer/assets/images/power.png$image
www.macobserver.com##.stock-widget.widget
www.macobserver.com###display_ad_widget-11
www.macobserver.com###display_ad_widget-10
www.macobserver.com###display_ad_widget-8
www.macobserver.com###display_ad_widget-9
www.macobserver.com###display_ad_widget-12
www.macobserver.com###custom_html-2:has-text(Support TMO)
www.techhive.com##[href="http://www.techhive.com/newsletters/signup.html"]
www.macobserver.com##.post-newsletter:has(#tmo-newsletter-form)
www.techhive.com##.promoBannerFrame
www.techhive.com###amazon-bottom-widget
www.techhive.com##[href^="https://www.pcworld.com/couponcodes"]
www.techhive.com##aside.emo-sb.emo
foundation.mozilla.org##.donate-modal-wrapper
developer.mozilla.org###contribution-popover-container
www.mozilla.org##.notification-banner:has-text(Your Firefox is out-of-date.)
www.ghacks.net##[href="https://deals.ghacks.net/"]
www.androidpolice.com###aprecentpostswidget-2:has(.section-header:has-text(Latest Deals))
www.extremetech.com##.articles > li:has([href^="https://www.extremetech.com/deals/"])
www.aarp.org##.aarpe-discounts-and-offers
nationalpost.com###utilitysettings-6:has-text(Share your feedback):has-text(Take our 60-second survey)
imgur.com##.post-action-tags
lowendbox.com###menu > div.block  > div:has(a[href^="https://buysellads.com/"])
gfycat.com##.iframe__bottom
##.akismet_comment_form_privacy_notice
torrentz2.eu###recent
stackoverflow.com##._hero.s-signup > #openid-buttons > ._facebook.btn
thenextweb.com##.with-tooltip.post-actions-shortUrl
console.scaleway.com##.e1cu1im40.css-q919en-FloatingButton--FloatingButton
@@||connect.facebook.net$domain=www.verkkouutiset.fi
@@||facebook.com/connect$domain=www.verkkouutiset.fi
@@||facebook.com/plugins/comments.php$domain=www.verkkouutiset.fi
##.comment-likes.comment-likes-widget-placeholder.likes-widget-placeholder
##a[href^="https://support.twitter.com/"][title="Twitter Ads info and privacy"]
www.yify-subtitles.com###dbox:has(.dbox_dialog)
github.com##.user-status-header
github.com##.js-user-status-context
github.com##.user-status-container
metro.co.uk##li.post.item:has(.metro-signpost-ad-feature)
metro.co.uk##li.metro-discount-codes-
nypost.com##.outbrain-enabled
www.cultofmac.com##[src="/wp-content/themes/com2014/images/sidebar_cultcast_v2.gif"]
||www.cultofmac.com/wp-content/themes/com2014/images/sidebar_buyback.gif$image
||www.cultofmac.com/wp-content/themes/com2014/images/sidebar_buyback.gif$image
temp-mail.org##.bottom_news_block
||temp-mail.org/images/plugins-banners/firefox_temp_mail_130x94.png$image
temp-mail.org##.side-plugin-banner
webcache.googleusercontent.com/favicon.ico$image
knowyourmeme.com##.floating-share-bar
www.whois.com##.chiclet_wph
www.whois.com##.slides-container
www.whois.com###suggest-container
www.whois.com##.promoblurb-container
www.laweekly.com##em:has([href="http://www.facebook.com/LAWeeklyMusic"])
www.laweekly.com##.bottom-lists:has-text(From Our Sponsors)
||searchengineland.com/figz/wp-content/themes/searchengineland/img/icons/black-badge-appstore.png$image
||searchengineland.com/figz/wp-content/themes/searchengineland/img/icons/black-google-play-badge.png$image
sourceforge.net##.bitdefender-promo
sourceforge.net##.col-bitdefender
www.liquidweb.com##[href="https://www.liquidweb.com/campaign/KB75Trial/"]:has(img)
my-summer-car.wikia.com###WikiaArticleComments
my-summer-car.wikia.com###wikia-recent-activity
alternativeto.net##.sponsored
www.omgubuntu.co.uk##.ChartWidget-row--red.ChartWidget-row:has-text(Hannah Montana Linux)
www.androidpolice.com##.menu-item > [href="https://www.androidpolice.com/subscribe-donate/"]
##a[href^="https://www.digitalocean.com/?refcode="]
raymii.org##[href^="http://clients.inceptionhosting.com/aff.php"]
||onavo.com^
||play.google.com/store/apps/details?id=com.onavo.*$document
www.tomsguide.com###carousel-multi-css--reponsive-table-532456-
www.occrp.org##.mfp-ready.mfp-auto-cursor.mfp-close-btn-in.mfp-wrap
www.occrp.org##.mfp-ready.mfp-bg
www.occrp.org##html:style(overflow: auto !important;)
tools.pingdom.com##app-cta-bar
cloud.scaleway.com##.brand.topbanner > [href="https://console.scaleway.com/"]
digiex.net##.samSupportUs
www.tomshardware.com###newsletter
www.cnbc.com###nl-optin-rr-move
www.ctrl.blog##.cta-email.page-footer-cta
medium.com,hackernoon.com##.overlay--lighter.overlay
bbs.io-tech.fi##.sidebar> p:has([href^="https://hinta.fi/"])
bbs.io-tech.fi###huDfpgkE > [href^="https://hinta.fi/"]
old.reddit.com##.linkinfo > .shortlink
qz.com###article-short-url
||forum.xda-cdn.com/images/suggested-apps/play-store-icon-icon.png$image
old.reddit.com##.premium-banner
linuxg.net##.widget_patreon_sidebar_site_widget
linuxg.net##.cb_p6_patreon_button
imgur.com###right-content
www.quora.com##.promoted_answer_wrapper
thebarentsobserver.com##.adsense
boingboing.net###next-post-thumbnails
boingboing.net##.widget_stackcommerce_widget
theintercept.com##.InterceptedBanner--desktop.InterceptedBanner
www.apkmirror.com###sidebar
www.youtube.com###unlimited-guide-item
www.youtube.com###-guide-item
@@connect.facebook.net/nl_NL/sdk.js$domain=538.nl
538.nl#@#.myTestAd
searx.me##.infobox_part.table-striped.table > tbody > tr:has(td:has-text(Instance of))
searx.me##a[href^="https://searx.me:3000/?mortyurl="]
nmap.org##li:nth-of-type(8) > [href="https://insecure.org/advertising.html"]
||blockchain.info/Resources/loading-large.gif$image
searx.me##div.panel-default.panel:has(.panel-heading > .panel-title:has-text(Donate))
||tenta.com/test/images/google-play-badge.png$image
cock.li##.infinite-scrolling
quellish.tumblr.com##.open.tmblr-iframe--follow-teaser.tmblr-iframe
securityheaders.com##div.reportSection:has-text(Supported By)
10minutemail.net###support-table
spectrum.ieee.org###slashpage
www.whatismybrowser.com##.wideskyscraper160.nonresponsive.fun
www.whatismybrowser.com##.lrec336.nonresponsive.fun
www.whatismybrowser.com##.responsive.fun
www.whatismybrowser.com##.text > [href="/out/nordvpn"]
www.whatismybrowser.com##.matched-content-page.matched-content.nonresponsive.fun > .fun-content
www.whatismybrowser.com##.matched-content-page.matched-content.nonresponsive.fun > .fun-credits
www.whatismybrowser.com##.fun-block.content-block
forums.mydigitallife.net##.noticeContent.baseHtml:has-text(This site uses cookies.)
||api.newsguardtech.com/check?$xhr
||voidlinux.eu^
www.serverhunter.com##.advertisement
www.serverhunter.com##.advertisement.offer-row
www.dailycal.org##.entry-related
www.dailycal.org##.donation-box-container
www.dailycal.org##.supporter-container
@@||search.lelux.fi^$generichide
@@||haku.lelux.fi^$generichide
@@||searx.me^$generichide
@@||searx.xyz^$generichide
@@||searx.nixnet.xyz^$generichide
miningpoolstats.stream##[href="https://www.binance.je"]:has(img[src*="referral"])
||miningpoolstats.stream/img/zhash_60_2.jpg$image
miningpoolstats.stream##[href="https://bitforex.com"]:has(img)
||miningpoolstats.stream/img/poolin_300.gif$image
||miningpoolstats.stream/img/equi462_v1.gif$image
||cdn.report-uri.com/libs/report-uri-js/^$script
browserleaks.com###comments
||disqus.com^$domain=browserleaks.com
browserleaks.com##.vpn
