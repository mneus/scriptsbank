||taboola.com^$3p
||google-analytics.com/analytics.js$script,redirect=google-analytics.com/analytics.js:5
||google-analytics.com/cx/api.js$script,redirect=google-analytics.com/cx/api.js:5
||lnkr.us^$doc
||icontent.us^$doc
||qip.ng^$doc
||qip.ru^$doc
||ratexchange.net^
||adnotbad.com^
||serverads.net^
||jsfuel.com^
||takethatad.com^
||tradeadsexchange.com^
||googletagservices.com/tag/js/gpt.js$script,redirect=googletagservices.com/gpt.js:5
||securepubads.g.doubleclick.net/tag/js/gpt.js$script,redirect=googletagservices_gpt.js:5
||pagead2.googlesyndication.com/tag/js/gpt.js$script,redirect=googletagservices_gpt.js:5
||scorecardresearch.com/beacon.js$script,redirect=scorecardresearch.com/beacon.js:5
||google-analytics.com/ga.js$script,redirect=google-analytics.com/ga.js:5
||addthis.com^$important,3p,domain=~amd.com|~missingkids.com|~missingkids.org|~sainsburys.jobs|~schonmagazine.com|~sitecore.com|~plotaroute.com
||addthis.com/*/addthis_widget.js$script,redirect=addthis.com/addthis_widget.js
##.addthis_toolbox
free18.net,gadgetlove.com,nrc.gov,onbeing.org,rapgenius.com,schonmagazine.com,tech.co,tmz.com#@#.addthis_toolbox
@@||addthis.com/js/*/addthis_widget.js$script,domain=amd.com
@@||addthisedge.com/live/$script,domain=amd.com
@@||addthis.com^$script,domain=schonmagazine.com
@@||addthis.com/js/*/addthis_widget.js$script,domain=plotaroute.com
||googletagmanager.com/gtm.js$script,redirect=googletagmanager_gtm.js:5
||widgets.outbrain.com/outbrain.js$script,redirect=outbrain-widget.js:5,domain=~mainichi.jp|~vice.com
||google-analytics.com/analytics.js$important,script,redirect=google-analytics.com/analytics.js,domain=support.amd.com
||googletagmanager.com/gtm.js$important,script,redirect=googletagmanager.com/gtm.js,domain=support.amd.com
teamskeet.com##+js(set, $.tstracker, noopFunc)
rediff.com##+js(ra, onclick, [onclick^="track"])
rediff.com##+js(ra, onmousedown, [onmousedown^="return enc(this,'https://track.rediff.com"])
||reddit.com/api/jail/$xhr,1p
||colpirio.com^$3p
docs.google.com##+js(no-xhr-if, method:POST url:/logImpressions)
liberation.fr,officedepot.fr,oui.sncf##+js(acis, document.createElement, '.js')
sfr.fr##+js(aopr, _oEa)
brillen.de##+js(acis, document.createElement, 'script')
||marketing.net.*^$1p
||vidtech.cbsinteractive.com^*/tracking/$script,redirect=noop.js,important
||carsensor.net/usedcar/modules/clicklog_$xhr,1p,important,redirect=noop.txt
/analytics/analytics.$~xmlhttprequest,3p
/ga_setup.js$3p
/googleanalytics.js$3p
-google-analytics/$domain=~wordpress.org,badfilter
-google-analytics/$3p,domain=~wordpress.org
||the-japan-news.com/modules/js/lib/fgp/fingerprint2.js$script,redirect=fingerprint2.js,important
||mtsa.com.my/mtcs.php/pageview/track^$image
||api.tumblr.com/*/share/stats$script,3p
frogogo.ru##+js(aopw, ADMITAD)
||artfut.com/static/tagtag.$script,3p,redirect=noop.js
||collector.xhamster*.*^
||copyhomework.com^
||coursecopy.com^
||quiztoolbox.com^
||quizlookup.com^
||studyeffect.com^
||testbooksolutions.com^
@@||googletagmanager.com/gtm.js$script,redirect-rule,domain=rocketnews24.com
/stats.php?*event=$image
/check.php?t=*&rand=$image,1p
/jquery.js?*&rx=*&foxtail=$image,1p
||jsdelivr.net/npm/skx@*/optical.js
/counter/?domain=$image,1p
||hd21.com/ajax/track?
||drtuber.*/ajax/track?track_type=
beaumontenterprise.com,chron.com,ctinsider.com,ctpost.com,expressnews.com,houstonchronicle.com,lmtonline.com,middletownpress.com,mrt.com,newstimes.com,nhregister.com,registercitizen.com,sfchronicle.com,stamfordadvocate.com,thehour.com,timesunion.com##+js(cookie-remover, realm.cookiesAndJavascript)
||playbrain.io/analytics/
tweakers.net##+js(set, WebTrekkClickMap, noopFunc)
tweakers.net##+js(aopr, snowplow)
tweakers.net##+js(acis, AbStats, trackId)
tweakers.net##+js(aopr, GlobalSnowplowNamespace)
tweakers.net##+js(set, tweakersConfig.wtConfig.scriptUrl, '')
tweakers.net##+js(aost, btoa, send)
||yuktamedia.com^$3p
||gamedock.io^$3p
*$script,redirect-rule=noopjs,domain=kruidvat.nl
kruidvat.nl##.async-hide:style(opacity:1.0 !important)
||stats.webgames.io^
||sporizle1.pw/embed/*?stat=$frame
||blogfoster.com^$3p
||myanalytic.net^$3p
||t.simply-hentai.com^
search.brave.com##+js(no-fetch-if, body:browser)
||d3bch4rrbnbe5n.cloudfront.net/pxl.png^
/visilabs.min.js
||civicscience.com^$3p
||smallseo.tools/rainbow/track$xhr
/^\w+://[\w.\-]+\.www\.woodbrass\.com//$1p,script
/dataunlocker$script,1p,domain=~dataunlocker.com
/^\w+://[0-9a-z]{12}\.dataunlocker\.com//$1p,script
/^\w+://[\w.\-]+\.www\.cybernetman\.com//$1p,script
/^\w+://[\w.\-]+\.www\.logology\.co//$1p,script
/^\w+://[\w.\-]+\.www\.atlaslane\.com//$1p,script
/^\w+://[0-9a-z]{12}\.tapmyback\.com//$1p,script
fjlaboratories.com##+js(aost, Math.floor, injectedScript)
stoic.ai##^script:has-text(\x3c)
stoic.ai##+js(aost, Math.floor, injectedScript)
||ampl.cinotes.com^
||data-saver-cindi.herokuapp.com^
||cdn.cindicator.com/$script,3p
botcomics.com,chandlerorchards.com,comicleaks.com,filmustage.com,freecodecamp.org,mailfloss.com,marketdata.app,monumentmetals.com,nikita.tk,ping.gg,cefirates.com##+js(acis, String.fromCharCode, join)
botcomics.com,chandlerorchards.com,comicleaks.com,marketdata.app,monumentmetals.com,nikita.tk,cefirates.com##^script:has-text(join)
nikita.tk##+js(aost, document.cookie)
||nikita.tk/*-$script,1p
||washpost.nile.works^
/p13n/batch/action/*$image
||da29e6b8-f018-490f-b25f-39a887fc95e7.xyz^
/^https:\/\/cdn\.jsdelivr\.net\/npm\/[-a-z_]{4,22}@latest\/dist\/script\.min\.js$/$script,3p,match-case
||1ee085aa-4c05-43a8-a4fe-fafedbedd062.xyz^
||03c844c9-d265-4006-a39d-400e6cb40bb7.xyz^
||connectier.io^
||nsfw.xxx/vendor/fingerprint/fingerprint2.min.js$script,redirect=fingerprint2.js,important
||collector.xhwebsite.com^
||collector.hamsterix.*^
||collector.xhwide1.com^
||g.jwpsrv.com/g/gcid-*?notrack$frame
tacobell.com##+js(set, bmak.js_post, false)
||cloudflare.com/ajax/libs/fingerprintjs2/$script,redirect=fingerprint2.js,important,domain=gamebox.gesoten.com
||gamerch.com/s3-assets/library/js/fingerprint2.min.js$script,redirect=fingerprint2.js,important
||ahentai.top/counter.php
reddit.com##+js(no-fetch-if, url:/^https:\/\/www\.reddit\.com$/ method:post)
reddit.com##+js(no-xhr-if, method:POST url:/^https:\/\/www\.reddit\.com$/)
||tr.jianshu.com^
/lib/f_ad_code.js
||159.203.84.58^
||cqrvwq.com^
/\.com\/[-_0-9a-zA-Z]{4,}\/[-\/_0-9a-zA-Z]{25,}$/$script,1p,domain=gu-global.com|uniqlo.com
||metrics.surinenglish.com^
||count.candou.com^
@@||natureetdecouvertes.com^*/pixel.png$~third-party,badfilter
||cm.bilibili.com/cm/api/$xhr
||wannads.com/api/track/fingerprint^
||wuzhuiso.com^$removeparam=src
||statwup.huya.com^
||va.huya.com^
||e-stat.huya.com^
||analytics.tiendaenoferta.com^
||zhihu.com^$removeparam=hybrid_search_source
||zhihu.com^$removeparam=hybrid_search_extra
skk.moe##+js(no-fetch-if, method:POST)
/cfga/jquery.js?$image
/npm/sks@0.*/lazyload.js$script,3p
/s/js/ta-2.3.js?
||rjno1.com/cdn-cgi/challenge-platform/h/b/scripts/invisible.js
||mynewsmedia.co/*/Linkpage/ads_stats_controller.php
||gplinks.co/Auth/ads_stats_controller.php
||videovard.*/api/front/view^$xhr,important
endbasic.dev,jmmv.dev##+js(no-xhr-if, method:POST)
||b90.yahoo.co.jp^
||jsdelivr.net^*/fp.min.js$script,redirect-rule=fingerprint3.js:10
/log/*$xhr,domain=vizcloud.*|vizcloud2.*
||serasaexperian.com.br/dist/scripts/fingerprint2.js^$redirect=fingerprint2.js,script,important
old.reddit.com##+js(ra, data-outbound-url, .outbound)
reddit.com##+js(set, Object.prototype.allowClickTracking, false)
||a2f41194651173ebf.awsglobalaccelerator.com^$3p
/?p=%2F*&h=https%3A%2F%2F*&r=&sid=*&qs=*&cid=$image,1p
/?h=https%3A%2F%2F*&r=&sid=*&qs=*&cid=$image,1p
webtoons.com##+js(set, ccsrv, '')
webtoons.com##+js(set, lcs_SerName, '')
||hdslb.com/bfs/cm/cm-sdk/static/js/bili-collect.js$script,redirect=noop.js,domain=bilibili.com,important
||imsdb.pw/player/ip.php
tarnkappe.info##+js(aopr, Matomo)
salsaposten.no,steinkjer-avisa.no,hamar-dagblad.no,lofot-tidende.no,avisa-valdres.no,amta.no,firdaposten.no,ringblad.no##+js(nostif, waiting) 